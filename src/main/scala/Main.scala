import akka.actor.{ActorSystem, Props}
import models.app.Manager

object Main {
  implicit val system = ActorSystem("my-system")
  def main(args: Array[String]): Unit = {
    system.actorOf(Props[Manager], "manager")
  }
}

