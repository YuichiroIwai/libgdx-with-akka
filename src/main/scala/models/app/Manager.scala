package models.app

import akka.actor.{Actor, ActorRef, Cancellable, PoisonPill, Props}
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.typesafe.config.ConfigFactory
import models.ball.{Ball, SimpleBall}
import models.cannon.{Cannon, CannonActor}
import models.enemy.{Enemy, EnemyActor, EnemyManager}
import models.events._
import models.vobjs._

import scala.collection.concurrent.TrieMap
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

class Manager extends Actor {
  val config = ConfigFactory.load()
  val header = Area(0, config.getInt("app.window.height") - config.getInt("app.header.height"), config.getInt("app.window.width"), config.getInt("app.header.height"))
  implicit val stage = Area(0, 0, config.getInt("app.window.width"), config.getInt("app.window.height") - config.getInt("app.header.height"))
  implicit val grid = Grid(40)
  val enemyManager = context.actorOf(Props(classOf[EnemyManager], self, stage, grid))
  var f: Cancellable = context.system.scheduler.scheduleOnce(10.second, self, Timeout)
  val (app, model) = initialize()

  private def initialize() = {
    val listener = new AppListener(self)
    val cannons = TrieMap.empty[Tile, ActorRef]
    val enemies = TrieMap.empty[Lane, TrieMap[ActorRef, Enemy]]
    (new LwjglApplication(listener, "test", config.getInt("app.window.width"), config.getInt("app.window.height")), new ModelForwarder(listener, CachedEntities(cannons, enemies)))
  }
  def remove(actor: ActorRef, ball: Ball) = {
    model.remove(actor, ball)
    actor ! PoisonPill
  }
  def remove(actor: ActorRef, enemy: Enemy) = {
    model.remove(actor, enemy)
    actor ! PoisonPill
  }
  def add(ball: Ball) = {
    val actor = context.actorOf(Props(classOf[SimpleBall], self, ball, Speed(0, 3), stage))
    model.add(actor, ball)
  }
  def add(enemy: Enemy) = {
    val actor = context.actorOf(Props(classOf[EnemyActor], self, enemy, Speed(0, -1), stage))
    model.add(actor, enemy)
  }
  def update(currentTile: CurrentTile) = model.update(currentTile)
  def updateCurrentTile() = {
    val targetTilePos = grid.tile(Pos(Gdx.input.getX, stage.height + header.height - Gdx.input.getY)).toPos(grid)
    val currentTile = model.currentTile.map { t => t.updateTarget(targetTilePos); t.update() }.getOrElse(new CurrentTile(targetTilePos))
    update(currentTile)
  }

  override def receive: Receive = {
    case Add(entity) =>
      entity match {
        case ball: Ball => add(ball)
        case enemy: Enemy => add(enemy)
      }
    case Update =>
      f.cancel()
      f = context.system.scheduler.scheduleOnce(1.second, self, Timeout)
      context.actorSelection("/user/manager/*") ! Update
      updateCurrentTile()
    case Updated(entity) =>
      entity match {
        case ball: Ball =>
          if (stage.contain(ball.pos)) {
            model.update(sender, ball)
            model.getEnemies(grid.lane(ball.x)) foreach (enemyMap => enemyMap foreach {
              case (enemyActor, enemy) =>
                if (Math.abs(enemy.y - ball.y) < 10) {
                  // FIXME 暫定で1ダメージ
                  enemyActor ! Damage(1)
                  remove(sender, ball)
                }
            })
          } else remove(sender, ball)
        case cannon: Cannon =>
          model.update(sender, cannon)
        case enemy: Enemy =>
          if (enemy.isAlive) {
            model.update(grid.lane(enemy.x), sender, enemy)
            model.update(sender, enemy)
          } else remove(sender, enemy)
      }
    case TouchDown(x, y) =>
      val tile = grid.tile(Pos(x, stage.height + header.height - y))
      if (model.isDefinedAt(tile)) {
        // FIXME とりあえず消してみる
        val actor = model.get(tile)
        model.remove(actor, tile)
        actor ! PoisonPill
      } else {
        val pos = grid.fit(Pos(x, stage.height + header.height - y))
        val cannon = Cannon(pos.x + grid.size / 2, pos.y, Charge(0, 100, 0, 1))
        val actor = context.actorOf(Props(classOf[CannonActor], self, cannon, grid))
        model.add(actor, tile, cannon)
      }
    case Timeout =>
      println("Timeout!")
      context.system.terminate()
    case _ =>
  }
}
