package models.app

import akka.actor.ActorRef
import models.ball.Ball
import models.cannon.Cannon
import models.enemy.Enemy
import models.vobjs.{Grid, Lane, Tile}

import scala.collection.concurrent.TrieMap

class ModelForwarder(listener: AppListener, cachedEntities: CachedEntities)(implicit val grid: Grid) {

  import cachedEntities._
  import listener.model

  def isDefinedAt(tile: Tile) = cannons.isDefinedAt(tile)
  def getEnemies(lane: Lane) = enemies.get(lane)
  def get(tile: Tile) = cannons(tile)
  def currentTile = model.currentTile
  def add(actor: ActorRef, ball: Ball) = model.balls += actor -> ball
  def add(actor: ActorRef, enemy: Enemy) = {
    val lane = grid.lane(enemy.x)
    if (!enemies.isDefinedAt(lane)) enemies += lane -> TrieMap.empty[ActorRef, Enemy]
    enemies(lane) += actor -> enemy
    model.enemies += actor -> enemy
  }
  def add(actor: ActorRef, tile: Tile, cannon: Cannon) = {
    model.cannons += actor -> cannon
    cannons += tile -> actor
  }
  def remove(actor: ActorRef, tile: Tile) = {
    model.cannons -= actor
    cannons -= tile
  }
  def remove(actor: ActorRef, ball: Ball) = model.balls -= actor
  def remove(actor: ActorRef, enemy: Enemy) = {
    model.enemies -= actor
    enemies.get(grid.lane(enemy.x)) foreach (_ -= actor)
  }
  def update(currentTile: CurrentTile) = model = model.copy(currentTile = Some(currentTile))
  def update(actor: ActorRef, ball: Ball) = model.balls(actor) = ball
  def update(actor: ActorRef, cannon: Cannon) = model.cannons(actor) = cannon
  def update(actor: ActorRef, enemy: Enemy) = model.enemies(actor) = enemy
  def update(lane: Lane, actor: ActorRef, enemy: Enemy) = enemies(lane)(actor) = enemy
}

case class CachedEntities(cannons: TrieMap[Tile, ActorRef], enemies: TrieMap[Lane, TrieMap[ActorRef, Enemy]])
object CachedEntities {
  def empty = CachedEntities(TrieMap.empty, TrieMap.empty)
}