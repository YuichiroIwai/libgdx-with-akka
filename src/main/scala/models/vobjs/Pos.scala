package models.vobjs

case class Pos(x: Float, y: Float) {
  def map[R](f: (Float, Float) => R): R = f(x, y)
  def foreach(f: (Float, Float) => Unit): Unit = f(x, y)
}
