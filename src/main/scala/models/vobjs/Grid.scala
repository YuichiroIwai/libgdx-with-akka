package models.vobjs

case class Grid(size: Int, offsetX: Int = 0, offsetY: Int = 0) {
  def tile(pos: Pos): Tile = Tile(Math.floor(pos.x / size).toInt, Math.floor(pos.y / size).toInt)
  def lane(x: Float): Lane = Lane(Math.floor(x).toInt)
  def fit(pos: Pos): Pos = Pos(pos.x - (pos.x % size), pos.y - (pos.y % size))
}
