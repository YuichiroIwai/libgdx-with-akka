package models.cannon

import models.core.{Chargeable, Entity}
import models.vobjs.Charge

case class Cannon(x: Float, y: Float, charge: Charge) extends AnyRef with Chargeable[Cannon] with Entity[Cannon] {
  override def moved(dx: Float, dy: Float): Cannon = copy(x + dx, y + dy)
  override def charged(newCharge: Charge): Cannon = copy(charge = newCharge)
}
