package models.cannon

import akka.actor.{Actor, ActorRef}
import models.ball.Ball
import models.core.{Charger, UpdatableActor}
import models.events.{Add, Updated}
import models.vobjs.Grid

class CannonActor(val manager: ActorRef, cannon: Cannon, grid: Grid) extends Actor with UpdatableActor[Cannon] with Charger[Cannon] {
  override def initialize(): Cannon = cannon
  override def update(cannon: Cannon): Cannon = modified(cannon, charge, shot)
  def shot(cannon: Cannon): Cannon = if (cannon.charge.isMax) {
    manager ! Add(Ball(cannon.x, cannon.y + grid.size))
    cannon.flush()
  } else cannon
  override def updated(cannon: Cannon): Unit = manager ! Updated(cannon)
  override def receive: Receive = updateHandler
}
