package models.core

trait LivingEntity[E <: Entity[E]] {
  self : Entity[E] =>
  def damaged(damage: Int): E
  def isAlive: Boolean
}
