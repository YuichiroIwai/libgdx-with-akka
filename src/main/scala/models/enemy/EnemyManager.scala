package models.enemy

import akka.actor.{Actor, ActorRef}
import models.events.{Add, Update}
import models.vobjs.{Area, Grid, Life, Pos}

class EnemyManager(manager: ActorRef, stage: Area, grid: Grid) extends Actor {
  var counter = 0

  override def receive: Receive = {
    case Update =>
      counter += 1
      if (counter > 100) {
        counter = 0
        val pos = grid.fit(Pos((Math.random() * stage.width).toFloat, stage.height))
        manager ! Add(Enemy(pos.x + grid.size / 2, stage.height, Life(3, 3)))
      }
  }
}
