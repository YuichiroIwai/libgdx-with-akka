package models.enemy

import akka.actor.{Actor, ActorRef}
import models.core.{LivingActor, MovableActor, UpdatableActor}
import models.events.Updated
import models.vobjs.{Area, Speed}

class EnemyActor(val manager: ActorRef, enemy: Enemy, val speed: Speed, val area: Area) extends Actor with MovableActor[Enemy] with UpdatableActor[Enemy] with LivingActor[Enemy] {
  override def initialize(): Enemy = enemy
  override def update(enemy: Enemy): Enemy = modified(enemy, move)
  override def updated(enemy: Enemy): Unit = manager ! Updated(enemy)
  override def damaged(entity: Enemy, damage: Int): Enemy = entity.copy(life = entity.life.damaged(damage))
  override def receive: Receive = updateHandler orElse damageHandler
}
