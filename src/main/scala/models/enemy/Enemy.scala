package models.enemy

import models.core.{Entity, LivingEntity}
import models.vobjs.Life

case class Enemy(x: Float, y: Float, life: Life) extends Entity[Enemy] with LivingEntity[Enemy] {
  override def moved(dx: Float, dy: Float): Enemy = copy(x + dx, y + dy)
  override def damaged(damage: Int): Enemy = copy(life = life.damaged(damage))
  override def isAlive: Boolean = life.isAlive
}
