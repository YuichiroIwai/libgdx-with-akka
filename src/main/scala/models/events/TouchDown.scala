package models.events

case class TouchDown(x: Int, y: Int) extends AppEvent
