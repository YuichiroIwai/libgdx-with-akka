package models.events

import models.core.Entity

case class Add[T](entity: Entity[T]) extends AppEvent
